SELECT
i.item_id,
i.item_name,
i.item_price,
i1.category_name
FROM
item i
INNER JOIN
item_category i1
ON
i.category_id = i1.category_id
